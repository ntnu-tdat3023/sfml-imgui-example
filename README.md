# Example SFML application with ImGui and older style OpenGL

## Prerequisites
The C++ IDE [juCi++](https://github.com/cppit/jucipp) should be installed.

## Installing dependencies

### Debian based distributions
`sudo apt-get install libsfml-dev libglm-dev`

### Arch Linux based distributions
`sudo pacman -S sfml glm`

### OS X
`brew install sfml glm`

## Compiling and running
```sh
git clone --recursive https://gitlab.com/ntnu-tdat3023/sfml-imgui-example
juci sfml-imgui-example
```

Choose Compile and Run in the Project menu.
