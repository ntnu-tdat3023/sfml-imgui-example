#include "imgui-SFML.h"
#include "imgui.h"

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <SFML/System.hpp>

#ifdef __APPLE__
#include <OpenGL/glu.h>
#else
#include <GL/glu.h>
#endif

#include <glm/glm.hpp>

class SFMLApplication {
  sf::ContextSettings context_settings;
  sf::RenderWindow window;

public:
  SFMLApplication() : context_settings(24),
                      window(sf::VideoMode(800, 600), "SFML Example", sf::Style::Default, context_settings) {
    window.setFramerateLimit(144);
    window.setVerticalSyncEnabled(true);

    //Various settings
    glClearColor(0.5, 0.5, 0.5, 0.0);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    //Setup projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    gluPerspective(45.0, 4.0 / 3.0, 0.1, 100.0);

    glMatrixMode(GL_MODELVIEW);

    ImGui::SFML::Init(window);
  }

  void start() {
    glm::vec3 camera(0.0, 0.0, 6.0);
    sf::Clock deltaClock;

    bool show_triangle = true;
    float triangle_size = 1.0;

    bool running = true;
    while (running) {
      //Handle events
      sf::Event event;
      while (window.pollEvent(event)) {
        ImGui::SFML::ProcessEvent(event);
        if (event.type == sf::Event::KeyPressed) {
          if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            window.close();
            running = false;
          }
        }
        else if (event.type == sf::Event::MouseMoved) {
          camera.x = 0.01 * -(event.mouseMove.x - static_cast<int>(window.getSize().x) / 2);
          camera.y = 0.01 * (event.mouseMove.y - static_cast<int>(window.getSize().y) / 2);
        }
        else if (event.type == sf::Event::Closed) {
          window.close();
          running = false;
        }
      }

      //ImGui calls
      ImGui::SFML::Update(deltaClock.restart());

      ImGui::Begin("ImGui");

      if (ImGui::Button("Show/hide triangle"))
        show_triangle = !show_triangle;

      if (ImGui::SliderFloat("Change triangle size", &triangle_size, 0.0, 3.0)) {
      }


      ImGui::End();

      //Draw
      glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
      glLoadIdentity();

      gluLookAt(camera.x, camera.y, camera.z, //Camera position in World Space
                camera.x, camera.y, 0.0,      //Camera looks towards this position
                0.0, 1.0, 0.0);               //Up

      if (show_triangle) {
        glBegin(GL_TRIANGLES);
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(0.0, triangle_size, 0.0);
        glColor3f(0.0, 1.0, 0.0);
        glVertex3f(-triangle_size, -triangle_size, 0.0);
        glColor3f(0.0, 0.0, 1.0);
        glVertex3f(triangle_size, -triangle_size, 0.0);
        glEnd();
      }

      ImGui::Render();

      //Swap buffer (show result)
      window.display();
    }
  }
};

int main() {
  SFMLApplication sfml_application;
  sfml_application.start();

  return 0;
}
